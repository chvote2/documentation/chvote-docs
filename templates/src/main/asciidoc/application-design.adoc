= Application design
:imagesdir: ../images
:title-logo-image: image:common/background.jpg[]
:media: print
v0.X, 2018-XX-YY

:toc:

<<<
*Project Management Process*


[cols="30,70"]
|===
|Classification
|Not Public

|Status
|Preparation

|Project Number
|2450

|Project Name
|CHVote Platform and BackOffice

|Author
|

|Approvers, Auditors
|

|===

*Modifications, controls, approval*
[cols="15,15,40,30"]
|===
|Version |Date |Description, comments |Name or role

|
|
|
|

|===

*Definitions, acronyms and abbreviations*

[cols="15,15,70"]
|===
|Terms |Abbreviations |Definitions

|
|
|

|===

*References*

[cols="20,80"]
|===
|Distinctive sign |Title, source

|
|

|===

:sectnums:

<<<

== Document objectives

This document aims to describe the architecture and design of the XXX application.

== Introduction

TIP: Short introductory text

=== Application role in CHVote system

TIP: describe the application responsability in CHVote ecosystem.

=== Main use cases

TIP: Describe quickly its main use cases.

<<<
== Business Concepts

TIP : class diagram showing main business entities and their relationships, and a description of each business entity
and non trivial relationships. (or reference to an external document containing theses descriptions)

<<<
== Architecture

=== Architectural approach

TIP: describe here the main architecture properties and choices/drivers.

=== Interfaces

==== Environment diagram

TIP: show an environment diagram, using UML component diagram (component and provided interface symbols of plantuml).
This should show the application at the center, other applications/systems around it, and data flows between the
application under design and surrounding applications/systems.

==== Provided interfaces

TIP: List provided interfaces to other application/system. Provide a link to the interface contract documentation
(i.e. generated swagger document).

==== Required interfaces

TIP: List interfaces provided by other application/system that are required by this application.
For each interface, describe the provider (application/system name) and why this interface is required (its purpose)

=== Sub-systems and main components

TIP: This chapter lists all the main sub-systems and components involved in the application's architecture.
For a typical 3-tier application, a diagram could show frontend, backend and database sub-systems.
Another diagram could show main components of frontend, respectively backend.
Granularity is left to the writer's choice.
A description of each sub-system and component is expected.

=== Technologies

TIP: Describe what technologies are used.
For example, Angular, Material, etc.


<<<
== Security

TIP: this chapter describes the security concepts put in place.
Its content should be adapted to the application under design.

=== Data classification

TIP: For each business entity (or property), define : business owner and classification level.
Refer to the data classification directive explanation at the end of this document for more information.

=== Protection of data at rest

TIP: Describe here what measures are used to protect sensitive data at rest.
For example, stored electoral registers should be protected using encryption.
Typically, authentication data are usually stored after being hashed.

=== Access management

TIP: describe how user access is restricted (authentication and authorization mainly)

=== Input Validation

TIP: describe the input validation strategy and mecanisms. One can refer to system architecture document for general
considerations. What is expected here is the description of specific input validation mechanism.

=== Output Sanitization

TIP: describe the output sanitization strategy and mecanisms. One can refer to system architecture document for general
considerations. What is expected here is the description of specific output sanitization mechanism.

<<<
== Sub-system design

TIP: One chapter for each sub-system. The following chapter (Frontend, Backend, etc) are samples, useful for a typical
3-tier application.

=== Frontend

==== Main components

TIP: Describe what components are used, and what component are designed to be shared.
For each component, describe its name, objectives, major design principles, etc.
For example : framework used (Angular, Material, ...), shared services (i18n, http service, etc).

==== Model

TIP: Describe here the object model used by the sub-system. Describe critical choices made and their rational.
Only if needed according to complexity.
For example, view model description for chvote-receiver frontend as it is complex

=== Backend

==== Main components

TIP: Describe what components are used. and what component are designed to be shared
For each component, describe its name, objectives, major design principles, etc.

==== Model

TIP: Describe here the object model used by the sub-system. Describe critical choices made and their rational.
Only if needed according to complexity.

=== Database

==== Physical Data Model

TIP: ER diagram of database schema and description of each table and column is expected.

==== Estimate of data volume

TIP: identify which entity will be stored in large quantities and estimate volume of data.


<<<
== Critical or complex features design

TIP: This part aims to desribe the design of critical or complex features of the application.
The format is left to the appreciation of writer, and depends on the kind of solution used.
For example, some features of the vote receiver are critical, like identification of voter.
Some others are complex, like voter's ballot submission (and the associated protocol interactions).

TIP: The expected documentation elements are : introduction to the feature objectives,
constraints like security/performance/volume of data/etc requirements,
challenges to be adressed like multiple resources transaction, streaming, etc,
design of the solution and justification, known limitations if any.

TIP: One can use any UML diagrams to describe the solution, like class, sequence and state diagrams..

<<<

== Misc

=== Information classification directive (EGE-10-12_v2)

The purpose of data classification is to determine the required level of protection of information during its life cycle.

The directive defines 3 *levels of protection* of information :

. *Public* protection
. *Non-public* protection
. *Confidential* protection

In special cases, requiring a very strong protection, an optional 4th level can be used:

[start=4]
. *Secret* protection

The levels of protection, aimed at minimizing risks, are described as follow :

* *Public* protection:
** the risk is zero or minor for persons or corporations ('personne morale'),
** Minimum protection measures need to be implemented, including protecting information from inadvertent or malicious modification (low level integrity).
* *non-public* protection:
** persons or corporations ('personne morale') may be affected by the disclosure or loss of integrity of the information,
** standard protection measures are implemented, including protecting the information against unintentional or malicious disclosure or modification (medium to strong integrity).
* *Confidential* protection:
** the physical, moral, legal or economic situation of a person or corporation ('personne morale') or the reputation of the State may be seriously affected,
** Important protection measures, based on best practices, should be implemented (medium to strong integrity).
* *Secret* protection (optional and exceptional level):
** the life of a person or the security of the State may be endangered,
** Specific, organizational and / or technical, enhanced protection measures must be implemented.


*Protective measures* are associated to each protection level.

A protection level is selected based on 2 axis :

- Transparency
- Data protection

*Transparency axis*:

- public: the data, information and documents can be accessed by any natural or legal person, according to the applicable legal framework
- non-public - simple secret: information subject to the secrecy of function,
- non-public - qualified or special secret (taxation, medical, police forces records, etc.): special cases of secrecy
related to specific activities or areas.

In the case of information covered by a double secret, the most restrictive secret applies.

*Data protection axis*:

- _without personal data_,
- _with personal data_: any information relating to an identified or identifiable person or corporation ('personne morale'),
- _with sensitive personal data_: any personal data specifying the societal, political, legal, medical or other characteristics of the person;
  the personality profile is assimilated in this directive to a sensitive personal data.

*Classification table*

[cols="2,3,3,4",width="100%"]
|====
4+^|_Data protection axis_
^|_Transparency axis_ |without personal data |with personal data |with sensitive personal data

|Public |*Public* |_Not applicable_ |_Not applicable_
|Non Public - simple secret |*Non public* |*Non public* |*Confidential*
|Non Public - qualified or special secret |*Confidential* |*Confidential* |*Confidential*

|====