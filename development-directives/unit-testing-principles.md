Introduction
=============
This document defines criteria useful to guide a developer in identifying unit tests.

Rules are given based on code module category. 

Backend project
===============

Repository module
-----------------
- DO NOT unit test :
    - Entities : because there should not be any logic in it
    - Simple Spring-data-jpa generated methods, like findAll, findOne, update, etc.
- DO WRITE unit tests for :
    - hand-written query (JPAQL, SQL, Criteria, etc )
    - Complex Spring-data-jpa methods : More than 2 operators (AND, OR, GROUP BY...) or entities used 
- Advices :
    - Use code to create datasets in database, instead of using SQL query : its easier to maintain, IDE can spot impacted datasets when refactoring

Service module
--------------
- DO NOT unit test :
    - Value Objects : they should not contain any logic 
    - Exception classes : one should unit test the service triggering a business exception, not the business exception alone
- DO WRITE unit test when :
    - all public methods of a Service class should be tested
- Advices :
    - when writing interaction based assertion, only check targeted interaction, but do not assert implementation details : this will facilitate refactoring and evolution
    - externalize document datasets (like XML document) in dedicated files

REST module
-----------
- DO NOT unit test :
    - Controllers : they should not contain any logic. End to end tests are checking those endpoints.
- DO WRITE unit test when :
    - a controller has to handle some logic. It should be exceptionnal and documented.

FrontEnd project
================

- DO NOT unit test :
    - http services : they should not contain any logic
- DO WRITE unit test when :
    - services having some logic inside
    - component classes


