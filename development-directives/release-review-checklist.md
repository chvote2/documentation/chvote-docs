Introduction
============
This document regroups all reviews specifically defined for CHVote that
must be conducted before any release.

Security Checklist
==================

The checklist below is based on the following material:
- [OWASP code review guide](https://www.owasp.org/images/5/53/OWASP_Code_Review_Guide_v2.pdf)
- [SANS SWAT Checklist](https://software-security.sans.org/resources/swat)

Error handling and logging
--------------------------
- [ ] **Display Generic Error Messages**<br/>
Error messages should not reveal details about the internal state of the application. For example, file system path 
and stack information should not be exposed to the user through error messages.
- [ ] **No Unhandled Exceptions**<br/>
Given the languages and frameworks in use for web application development, never allow an unhandled exception to occur.
Error handlers should be configured to handle unexpected errors and gracefully return controlled output to the user.
- [ ] **Suppress Framework Generated Errors**<br/>
Your development framework or platform may generate default error messages. These should be suppressed or replaced with
customized error messages as framework generated messages may reveal sensitive information to the user.
- [ ] **Ensure a default error page is defined**<br/>
Ensure a default error page is defined ant that it does not leak any information on the server product, version or
any infrastructure information.
- [ ] **Log All Authentication Activities**<br/>
Any authentication activities, whether successful or not, should be logged.
- [ ] **Log All Privilege Changes**<br/>
Any activities or occasions where the user's privilege level changes should be logged.
- [ ] **Log Administrative Activities**<br/>
Any administrative activities on the application or any of its components should be logged.
- [ ] **Log Access to Sensitive Data**<br/>
Any access to sensitive data should be logged.
- [ ] **Log validation failures**<br/>
Ensure input validation and output validation failures are logged.
- [ ] **Do Not Log Inappropriate Data**<br/>
While logging errors and auditing access is important, sensitive data should never be logged in an unencrypted form,
e.g. session identification (JWT), personal data, passwords, private keys, database connection strings, etc.
- [ ] **Store Logs Securely**<br/>
Logs should be stored and maintained appropriately to avoid information loss or tampering by intruder. Log retention
should also follow the retention policy set forth by the organization to meet regulatory requirements and provide
enough information for forensic and incident response activities.
- [ ] **Sanitize log messages**<br/>
Ensure log message are sanitized so that a specifically crafted log message cannot be targeted to the log monitoring
tools (SIEM for example). 

Data Protection
---------------
- [ ] **Use HTTPS Everywhere**<br/>
Ideally, HTTPS should be used for your entire application. If you have to limit where it's used, then HTTPS must be
applied to any authentication pages as well as to all pages after the user is authenticated. If sensitive information 
(e.g. personal information) can be submitted before authentication, those features must also be sent over.
- [ ] **Disable HTTP Access for All Protected Resources**<br/>
For all pages requiring protection by HTTPS, the same URL should not be accessible via the insecure HTTP channel.
- [ ] **Use the Strict-Transport-Security Header**<br/>
The Strict-Transport-Security header ensures that the browser does not talk to the server over HTTP. This helps reduce
the risk of HTTP downgrade attacks as implemented by the sslsniff tool.
- [ ] **Securely Exchange Encryption Keys**<br/>
If encryption keys are exchanged or pre-set in your application then any key establishment or exchange must be
performed over a secure channel.
- [ ] **Set Up Secure Key Management Processes**<br/>
When keys are stored in your system they must be properly secured and only accessible to the appropriate staff on a
need to know basis.
- [ ] **Weak TLS Configuration on Servers**<br/>
Weak ciphers must be disabled on all servers. For example, SSL v2, SSL v3, and TLS protocols prior to 1.2 have known
weaknesses and are not considered secure. Additionally, disable the NULL, RC4, DES, and MD5 cipher suites. Ensure all
key lengths are greater than 128 bits, use secure renegotiation, and disable compression.
<br/>EXAMPLE: Qualys SSL Labs
- [ ] **Use Valid HTTPS Certificates from a Reputable CA**<br/>
HTTPS certificates should be signed by a reputable certificate authority. The name on the certificate should match the
FQDN of the website. The certificate itself should be valid and not expired.
- [ ] **Disable Data Caching Using Cache Control Headers and Autocomplete**<br/>
Browser data caching should be disabled using the cache control HTTP headers or meta tags within the HTML page.
Additionally, sensitive input fields, such as the login form, should have the autocomplete=off setting in the HTML form
to instruct the browser not to cache the credentials.
- [ ] **Limit the Use and Storage of Sensitive Data**<br/>
Conduct an evaluation to ensure that sensitive data is not being unnecessarily transported or stored. Where possible,
use tokenization to reduce data exposure risks.

Configuration and operation
---------------------------
- [ ] **Perform Security Testing**<br/>
Conduct security testing both during and after development to ensure the application meets security standards. Testing
should also be conducted after major releases to ensure vulnerabilities did not get introduced during the update 
process.
- [ ] **Educate the Team on Security**<br/>
Training helps define a common language that the team can use to improve the security of the application. Education
should not be confined solely to software developers, testers, and architects. Anyone associated with the development
process, such as business analysts and project managers, should all have periodic software security awareness training.

Session Management
------------------
- [ ] **No session cookie**<br/>
Ensure that no cookie is used to maintain user session (we use JWT token)
- [ ] **Stateless architecture, no backend session**<br>
The backend should not store any data in HttpSession object

Input and output handling
-------------------------
- [ ] **Validate all inputs**<br/>
All external input to the system (and between applications) should undergo input validation. When possible, type, 
length, format and range should be checked. 
- [ ] **Validate server-side**<br/>
Ensure that data validation is applied server-side
- [ ] **Validation XML against a schema**<br/>
Any XML document provided as input should be validated against an XSD schema
- [ ] **Do not initialize bean before JSON binding**<br/>
Value Object, mapped from JSON payloads, must have only those instance variables that are dependent on the user inputs.
Review the application design and check if it incorporates a data binding logic. In case it does, check if business
objects/beans that get bound to the request parameters have unexposed variables that are meant to have static values. 
If such variables are initialized before the binding logic some attack would work successfully.
- [ ] **Validation cannot be circumvented**<br/>
Check there is no way to circumvent data validation.
- [ ] **Use angular sanitization services**<br/>
Angular has built-in sanitization of HTML/CSS/script/any input to prevent XSS attacks. If using another rendering 
engine/platform, outputs must be sanitized for that specific target platform, applying corresponding rules. 
- [ ] **Do not build URL with user input**<br/>
Ensure that no URL the browser get forwarded or redirected to is built with user provided input
- [ ] **Conduct Contextual Output Encoding**<br/>
All output functions must contextually encode data before sending it to the user. Depending on where the output will
end up in the HTML page, the output must be encoded differently. For example, data placed in the URL context must be
encoded differently than data placed in JavaScript context within the HTML page.
<br/>[EXAMPLE](https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet)
- [ ] **Prefer Whitelists over Blacklists**<br/>
For each user input field, there should be validation on the input content. Whitelisting input is the preferred
approach. Only accept data that meets a certain criteria. For input that needs more flexibility, blacklisting can also
be applied where known bad input patterns or characters are blocked.
- [ ] **Use Parameterized Queries**<br/>
JPA and SQL queries should be crafted with user content passed into a bind variable. Queries written this way are safe
against injection attacks. Queries should not be created dynamically using string concatenation. Similarly, the query 
string used in a bound or parameterized query should never be dynamically built from user input.
- [ ] **Use Tokens to Prevent Forged Requests**<br/>
For web application, check there's a CSRF protection mechanism in place. See https://angular.io/guide/security and
https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html
- [ ] **Set the Encoding for Your Application**<br/>
For every page in your application set the encoding using HTTP headers or meta tags within HTML. This ensures that the
encoding of the page is always defined and that browser will not have to determine the encoding on its own. Setting
a consistent encoding, like UTF-8, for your application reduces the overall risk of issues like Cross-Site Scripting.
- [ ] **Validate Uploaded Files**<br/>
When accepting file uploads from the user make sure to validate the size of the file, the file type, and the file
contents as well as ensuring that it is not possible to override the destination path for the file.
- [ ] **Use the Nosniff Header for Uploaded Content**<br/>
When hosting user uploaded content which can be viewed by other users, use the X-Content-Type-Options: nosniff header 
so that browsers do not try to guess the data type. Sometimes the browser can be tricked into displaying the data type 
incorrectly (e.g. showing a GIF file as HTML). Always let the server or application determine the data type.
- [ ] **Validate the Source of Input**<br/>
The source of the input must be validated. For example, if input is expected from a POST request do not accept 
the input variable from a GET request.
- [ ] **Use the X-Frame-Options Header**<br/>
Use the X-Frame-Options header to prevent content from being loaded by a foreign site in a frame. This mitigates 
Clickjacking attacks. For older browsers that do not support this header add framebusting Javascript code to mitigate
Clickjacking (although this method is not foolproof and can be circumvented).
- [ ] **Use Secure HTTP Response Headers**<br/>
The Content Security Policy (CSP) and X-XSS-Protection headers help defend against Cross-Site  Scripting (XSS) attacks.

Access control
--------------
- [ ] **Apply Access Controls Checks Consistently**<br/>
Always apply the principle of complete mediation, forcing all requests through a common security "gate keeper."
This ensures that access control checks are triggered whether or not the user is authenticated.
- [ ] **Apply the Principle of Least Privilege**<br/>
Make use of a Mandatory Access Control system. All access decisions will be based on the principle of least privilege.
If not explicitly allowed then access should be denied. Additionally, after an account is created, rights must be
specifically added to that account to grant access to resources.
- [ ] **Don't Use Direct Object References for Access Control Checks**<br/>
Do not allow direct references to files or parameters that can be manipulated to grant excessive access. Access control
decisions must be based on the authenticated user identity and trusted server side information.
- [ ] **Review every entry point for proper authorization**<br/>
Ensure that access to all functionality of application requires proper authorization. Every entry point should be reviewed.
- [ ] **Restrict user access to the data**<br/>
Our system being multi-tenant, ensure that the user can only access the data he is allowed to, according to the 
management entity he belongs to.
- [ ] **Centralize role names**<br/>
Role names should be defined in one place.
- [ ] **Issue HTTP 403 in case of failed authorization**<br/>
In cases where authorization fails, a HTTP 403 not authorized page should be returned.
- [ ] **Server-side authorization**<br/>
Authorization validation must be performed on server side.

Cryptography
------------
- [ ] **Use standard cryptography**<br/>
Ensure the cryptography primitives used are standards ones, no home-made cryptography should be written. 