# Contributing 

First off, thanks for taking the time to contribute! 

The following is a set of guidelines for contributing to CHVote and its packages.
These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document 
in a pull request.

#### Table Of Contents

[Code of Conduct](#code-of-conduct)

[What should I know before I get started?](#what-should-i-know-before-i-get-started)
  * [CHVote architecture and design](#chvote-architecture-and-design)
  * [Development directives](#development-directives)

[How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Suggesting Enhancements](#suggesting-enhancements)
  * [Forking](#forking)

## Code of Conduct

This project and everyone participating in it is governed by the [Code of Conduct](CODE_OF_CONDUCT.md). 

## What should I know before I get started?

### CHVote architecture and design

CHVote platform is made up of multiple online and offline applications, with specific role inside the global solution.

First, you should read the system architecture document...
then design document of the application / library...

TBC

### Development directives

...

## How Can I Contribute?

### Reporting Bugs

### Suggesting Enhancements

### Forking

[Follow the Project forking workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html)
TBC

#### Git Commit Messages

#### Merge request
