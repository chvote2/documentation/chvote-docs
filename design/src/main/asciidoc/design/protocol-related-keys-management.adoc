= CHVote Protocol related cryptographic keys management
:imagesdir: ../../images
:title-logo-image: image:common/background.jpg[]
:media: print
:icons: font
v0.4, 2018-07-12

:toc:

<<<
Project Management Process


[cols="30,70"]
|===
|Classification
|Public

|Status
|Preparation

|Project Number
|2450

|Project Name
|CHVote Platform and BackOffice

|Author
|C.Vigouroux

|Approvers, Auditors
|T. Hofer

|===

Modifications, controls, approval
[cols="15,15,40,30"]
|===
|Version |Date |Description, comments |Name or role

|0.1
|2018-06-28
|Initial revision
|Christophe Vigouroux

|0.2
|2018-07-03
|Inclusion of internal review feedback
|Christophe Vigouroux

|0.3
|2018-07-04
|Channel must be authenticated from CC to CC, U-Count does not need
to authenticate the CC messages.
|Christophe Vigouroux

|0.4
|2018-07-12
|The electoral public key is uploaded directly into the BackOffice
|Christophe Vigouroux

|===

*Definitions, acronyms and abbreviations*

[cols="22,18,60"]
|===
|Terms |Abbreviations |Definitions

|Identity and access management
|IAM
|Solution integrated into the CHVote platform which provides user identification, authentication, single-sign-on,
identity and permission management.

|===

*References*

[cols="20,80"]
|===
|Distinctive sign |Title, source

|
|

|===

:sectnums:

== Introduction

This document aims to describe the cryptographic key material needed as a dependency
by the voting protocol:

- the keys needed to sign and verify the exchanged protocol data
- the keys needed to encrypt and decrypt data between trusted parties over an insecure channel
- the keys needed to sign and verify data between trusted parties over an insecure channel

== Cryptographic keys design

=== Cryptographic keys

The following table defines the cryptographic keys needed by the voting protocol as dependencies
(i.e. the protocol does not instantiate them).

.Cryptographic keys definition table
[cols="10,15,20,10,30,15"]
|===
|Key Id|Key description|Type|Length|Usage|Storage format

|cc#j.n-s-sk
|Control component node signature private key
|Schnorr signature private key, value in identification group (Z~q_hat~);
|224 bits
|Signs messages produced by the control components. Each control
component node has its own signature private key.
|Raw key (byte array)

|cc#j.n-s-pk
|Control component signature public key
|Schnorr signature public key, value in identification group (G~q_hat~)
|2048 bits
|Used by client applications of the control components (U-Print, other Control Components, Bulletin Board,
PACT) to check the signature of the content sent by the control components. See
<<Cryptographic keys management processes>> for further information about
the provisioning of the public keys.
|Key converted to Base64 wrapped in JSON content. The key must be pinned to a specific component.
See <<Control components' public keys format>> for further details about the format.

|chvote-s-sk
|CHVote platform signature private key
|Schnorr signature private key, value in identification group (Z~q_hat~);
|224 bits
|Signs messages produced by the CHVote platform (PACT, Bulletin board).
|Raw key (byte array)

|chvote-s-pk
|CHVote platform signature public key
|Schnorr signature public key, value in identification group (G~q_hat~)
|2048 bits
|Used by the control components to check the signature of the content sent by the CHVote platform. See
<<Cryptographic keys management processes>> for further information about
the provisioning of the public key.
|Key converted to Base64 wrapped in JSON content. See <<CHVote platform signature public keys format>> for
further details about the format.

|pa-e-sk
|Printing authority decryption private key
|El-Gamal decryption private key, value in identification group (Z~q_hat~)
|224 bits
|Decrypts messages sent by the control components to the printing authority over an insecure channel.
|Raw key (byte array) wrapped in PKCS#12 and protected with password

|pa-e-pk
|Printing authority encryption public key
|El-Gamal encryption public key, value in identification group (G~q_hat~)
|2048 bits
|Encrypts control components messages to be sent to the printing authority. Uses
an hybrid encryption to encrypt any sized message: the printing authority encryption
public key is used to wrap a random 256 bits AES symmetric key, used to encrypt the
message using the AES-CTR algorithm.
See <<Cryptographic keys management processes>> for further information about
the provisioning of the public key.
|Key converted to Base64 wrapped in JSON content. The key must be pinned to a specific printing authority.
See <<Printing authority encryption or signature public key format>> for further details about the format.

|pa-s-sk
|Printing authority signature private key
|Schnorr signature private key, value in identification group (Z~q_hat~)
|224 bits
|Signs messages sent to the control components by the printing authority (voter identifiers archive)
over an insecure channel.
|Raw key (byte array) wrapped in PKCS#12 and protected with password

|pa-s-pk
|Printing authority signature public key
|Schnorr signature public key, value in identification group (G~q_hat~)
|2048 bits
|Used by the control components to check the signature of the content sent by the printing authority.
See <<Cryptographic keys management processes>> for further information about
the provisioning of the public keys.
|Key converted to Base64 wrapped in JSON content. The key must be pinned to a specific printing authority.
See <<Printing authority encryption or signature public key format>> for further details about the format.

|ea-e-sk#m
|Electoral authority ballot decryption private key share
|El-Gamal encryption private key share (n-out-of-m Shamir's secret sharing scheme),
 value in encryption group (Z~q~)
|224 bits
|n out of m electoral authority ballot decryption private key shares are used by U-Count to recombine the
key used to partially decrypt the ballots for the electoral authority. This partial decryption is
combined by U-Count with the partial decryptions of the control components to obtain the decrypted ballots.
|<Share index #m>:<key in base 64> wrapped as a byte array in PKCS#12 and protected with password

|ea-e-pk
|Electoral authority ballot encryption key part
|El-Gamal encryption public key, value in encryption group (G~q~)
|2048 bits
|Combined by the control components with the public encryption keys of each control
component (specific to each protocol instance) to generate the overall ballot encryption key
that is used by the voting client to cast the vote.
See <<Cryptographic keys management processes>> for further information about
the provisioning of the public key.
|Raw key (byte array)

|===

=== Storage and processing locations

The following schema summarizes the locations where the keys are stored.

NOTE: Keys outlined in orange are protected by a password (typically, PKCS#12 keystores). +
Keys outlined in green are in clear text.

.Storage locations schema
image::design/protocol-related-keys-management.png[Storage locations diagramm,pdfwidth=100%]

Further details about storage and processing are described in the following table:

.Storage and processing locations table
[cols="10,20,35,35"]
|===
|Key Id|Key description|Storage locations|Processing locations

|cc#j.n-s-sk
|Control component signature private key
a|* Local disk array of the control component node #j.n (important reminder: threat analysis
requires that the local disk array is encrypted using the SCSI controller).
* Control component application memory once started.
a|* Control component command line application: generates the key pair
* Control component application: signs the protocol messages

|cc#j.n-s-pk
|Control component signature public key
a|* Control components administrators' USB flash drive on key setup to transfer to the control
  components (creation or renewal).
* CHVote infrastructure git repository dedicated to public keys
* CHVote Business service manager encrypted USB flash drive
* CHVote application platform centralized configuration (OCP ConfigMap)
* Printing authority offline PC hard drive.
* Printing authority USB flash drive (different from the USB flash drive used to store the printing
authority private keys) on key setup to transfer the public keys from its
administrative platform (email access) to the Printing authority offline PC.
* Control component local disk array.
a|* Control component command line application: generates the key pair
* Control component application: verifies the other CC messages signatures
* U-Print: verifies the CC messages signatures
* PACT: verifies the CC messages signatures
* Bulletin Board: verifies the CC messages signatures

|chvote-s-sk
|CHVote platform signature private key
a|* CHVote platform centralized configuration, as an OCP secret whose access is restricted
to a specific role that is not given to the BackOffice application.
* PACT application memory once started.
* Bulletin board application memory once started.
a|* PACT command line application: generates the key pair
* PACT: signs the messages sent to the control components
* Bulletin board: signs the messages sent to the control components, except for the messages
originally sent by a control component and re-dispatched to all the control components, in this
case the original CC signal is kept.

|chvote-s-pk
|CHVote platform signature public key
a|* System administrator USB flash drive on key setup.
* Control components administrators' USB flash drive on key setup to transfer to the control
components (creation or renewal).
* Control component local disk array.
a|* PACT command line application: generates the key pair
* Control component application: verify the PACT or Bulletin board messages signatures

|pa-e-sk
|Printing authority decryption private key
a|* Printing authority USB flash drive.
* U-Print application memory.
a|* U-Print: generates the key pair, decrypts the CC voter data messages

|pa-e-pk
|Printing authority encryption public key
a|* Printing authority USB flash drive (different from the USB flash drive used to store the printing
authority private keys) on key setup to transfer the public keys from the Printing authority offline
PC to the administrative platform (email access).
* CHVote infrastructure git repository dedicated to public keys
* BackOffice database
* CHVote platform database
* Database of each control component (initialization parameters of the protocol instance).
a|* U-Print: key pair generation
* Back Office: forwards the printing authority template configuration containing the encryption
public key to the PACT.
* PACT: allows the public key to be reviewed by both the election collaborator and the election manager
when validating the election configuration and forwards to the control components when initializing the protocol.
* Control component application: encrypts the CC voter data messages to the printing authorities.

|pa-s-sk
|Printing authority signature private key
a|* Printing authority USB flash drive.
* U-Print application memory.
a|* U-Print: generates the key pair, signs the voter identifiers

|pa-s-pk
|Printing authority signature public key
a|* Printing authority USB flash drive (different from the USB flash drive used to store the printing
authority private keys) on key setup to transfer the public keys from the Printing authority offline
PC to the administrative platform (email access).
* CHVote infrastructure git repository dedicated to public keys
* BackOffice database
* CHVote platform database
* Database of each control component (initialization parameters of the protocol instance).
* IT Support USB flash drive on key setup to transfer to the CHVote application platform (creation or renewal).
a|* U-Print: generates the key pair
* Back Office: forwards the printing authority template configuration containing the signature public key
to the PACT.
* PACT: allows the public key to be reviewed by both the election collaborator and the election manager
when validating the election configuration and forwards to the control components when initializing the protocol.
* Control component application: verify the signature of the voter identifiers sent by the printing authorities.

|ea-e-sk#m
|Electoral authority ballot decryption private key share
a|* Electoral authority USB flash drive.
* U-Count application memory.
a|* U-Count: generates the private key shares, test the key shares, participates in the
decryption process of the ballots.

|ea-e-pk
|Electoral authority ballot encryption public key part
a|* BackOffice database
* CHVote platform database
* Database of each control component (initialization parameters of the protocol instance).
* Election officer USB flash drive on key setup to transfer the public keys from the Counting
offline PC to the administrative platform (BackOffice access).
a|* U-Count: generates the key pair
* Back Office: stores and forwards the electoral authority configuration containing the public key to the PACT.
* PACT: allows the public key to be reviewed by both the election collaborator and the election manager
when validating the election configuration and forwards to the control components when initializing the protocol.
* Control component application: combines the electoral authority public key with the control components
public key to generate the overall public key used by the client application to encrypt the votes.

|===

=== Implementation hints

==== Control components' public keys format

The control components' public keys should be provided as a single json file, including:

- the public keys of each control component node
- key pinning to each control component (useless to pin to a specific node)
- a way to support rotation of the keys.

This should be implemented using a json map:

[source,javascript]
----
{
  "cc0": [
    {
      "keyid": "cc0.1-20180624",
      "publicKey": "3cdef..."
    },
    {
      "keyid": "cc0.2-20180624",
      "publicKey": "4fedc..."
    },
    {
       // more keys --> key rotation support
    }
  ],
  ,
  "cc1": ...
}
----

==== Printing authority encryption or signature public key format

The printing authority encryption public keys should be provided as
a single json file, including:

- the currently valid public keys of the printing authority
- key pinning to the printing authority
- a way to support rotation of the keys.

This should be implemented using a json map:

[source,javascript]
----
{
  "<<printingAuthorityName>>": [
    {
      "keyid": "<<printingAuthorityName>>-20180624",
      "publicKey": "3cdef..."
    },
    {
       // more keys --> key rotation support
    }
  ]
}
----

where +<<printingAuthorityName>>+ is replaced by the actual printing authority identifier by
the U-Print application.

==== CHVote platform signature public keys format

The CHVote platform public key should be provided as a single json file, including:

- the currently valid public keys of the CHVote platform
- a way to support rotation of the keys.

This should be implemented using a json map:

[source,javascript]
----
{
  "chvote": [
    {
      "keyid": "chvote-20180624",
      "publicKey": "3cdef..."
    },
    {
       // more keys --> key rotation support
    }
  ]
}
----

== General requirements

=== Signatures for email communications

Whenever public keys are emailed from the owner of a component to other parties, a signature must be used to
ensure their authenticity. The signature must conform to the requirements of an _Advanced Signature_ with a SCSE
valid certificate provider (VEleS requirement #3.3.6).

Concretely, in the <<Cryptographic keys management processes>> descriptions, the _send a signed email_ action
consists in one of those two possibilities:

* either the public key file is signed in a dedicated tool then the signed file is attached in an email to
the CHVote IT service manager,
* or the public key file is attached to an email that is then signed by the email application and sent to
the CHVote IT service manager.

Furthermore, in the <<Cryptographic keys management processes>> descriptions, the _verify the signature
of the received email_ consists in verifying the signature at file or email level according to the technique used
to sign.

=== PKCS#12 best practices

Best practices are required to be used for key length and algorithms usages (VEleS requirement #3.3.6). This applies
to the PKCS#12 keystores used to secretly store private keys. The
https://pages.nist.gov/800-63-3/sp800-63b.html#memorized-secret-verifiers[NIST recommendations on password derivation]
and https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf[the NIST recommandations on passphrase length]
sets a baseline on this subject that is applied even more strictly for the CHVote system:

* PBKDF#2 iterations: as much as possible, at least 100.000 and within a random range.
* Salt size: at least 160 bits.
* User chosen password length: at least 20 characters for a passphrase (letters only)

=== Access restrictions to the Git repository dedicated to the public keys

The access to the CHVote infrastructure git repository dedicated to the public keys is
restricted by the CHVote internal IAM solution, ensuring that only the CHVote IT service manager can modify
this repository. Other parties who need an access to the public keys are given read-only permissions
by the IAM solution.

== Cryptographic keys management processes

TIP: Please refer to the "Actors catalog" chapter of the system architecture document for the full description
of the actors mentioned in the following processes.

=== Control components signature key pairs processes

==== General description

Each control component administrator pair must provide the public keys of the two nodes of the control components
under their charge to the other parties: U-Count users (electoral authorities), U-Print users (printer authorities),
and CHVote platform administrators.

For those parties to verify the authenticity of the public keys they received and use, they must trust some other
parties. Both the CHVote IT service manager and the CHVote Business service manager take this trusting authority role,
the separation of duty between them ensuring that a single person cannot tamper with the public keys provisioning.

Those trusting authorities represent an intermediate layer between the control components administrators and
the other parties, as it would practically be very hard to provide a secure communication between an ephemeral
control component administrators pair and a lot of parties scattered in the different cantons.

Practically, the following phases must be conducted:

* The control components public keys are given hand to hand to the CHVote IT service manager at
creation time.
* The CHVote IT service manager is then responsible to update the file of all the valid
public keys of the control components (including older public keys to support key rotations).
* In a face-to-face meeting, he hands over this file of all the valid public keys to the 4 control components
administrators pairs, who in turn check that they can find in the file the public keys of their own control components
nodes. During this meeting, the file is also given hand-to-hand to the CHVote Business service manager, who
secures it onto an encrypted USB flash drive.
* Finally, the CHVote IT service manager sends a signed email to the U-Print users with the new file of all
the valid public keys. Those users must then verify the signature and then call the CHVote Business service manager to
double-check the integrity and authenticity of the file by comparing the respective hash value of their files.

==== Control components signature key pairs creation

.Creation flow of the control components signature key pairs (only 2 control component administrators pairs represented)
image::generated/design/control-components-signature-key-pairs-flow.png[Control components signature key pairs creation flow]

:role: 4 distinct control component administrators pairs
include::role.part.adoc[]
. Go to one datacenter and connect to one node of a control component cluster
(using the physical control access and then logging to the local console).
. Execute a command line using the control component application binary
to execute the key pair generation console application and define the destination
directory of the keys to the local disk array.
. Copy the public key onto a dedicated, clean USB flash drive.
. Repeat steps 1 to 3 for the other datacenter.
. Meet with the CHVote IT service manager to give him hand-to-hand the public key.
. Repeat (optionally in parallel) steps 1 to 5 for the other control components with different administrators pairs.

:role: the CHVote IT service manager
include::role.part.adoc[]
[start=7]
. Add the public keys received hand-to-hand to the control components' public keys file stored in
the CHVote infrastructure git repository dedicated to the public keys. See
<<Control components' public keys format>> for the value format.
. Summon a meeting with the 4 distinct control component administrators pairs and the CHVote Business service manager.
. Give the control components' public keys file hand-to-hand to each control component administrators pair and to
the CHVote Business service manager.

:role: 4 distinct control component administrators pairs
include::role.part.adoc[]
[start=10]
. Check that the control components' public keys file contains valid public keys for the component administrators pair
own nodes. The overall process must be aborted in case of inconsistency.
. Copy the file onto the administrators pair USB flash drive used in step 3.

:role: the CHVote Business service manager
include::role.part.adoc[]
[start=12]
. Secure the control components' public keys file onto an encrypted USB flash drive.

:role: the CHVote IT service manager
include::role.part.adoc[]
[start=13]
. Send an email to the System administrator to signal that a new control components' public keys file is available
in the CHVote infrastructure git repository dedicated to the public keys.
. Send the new control components' public keys file to the printing authorities
(including the election officers acting as a test printing authority) in a signed email.

:role: the System administrator
include::role.part.adoc[]
[start=15]
. Get the public keys file from the CHVote infrastructure git repository dedicated to the public keys.
. Replace the old control components' public keys file with the new one in the OpenShift centralized
configuration.

:role: the Printing authorities
include::role.part.adoc[]
[start=17]
. Verify the signature of the received email containing the control components' public keys file.
. Copy the control components' public keys file from the email to the printing authority USB flash drive.
. Copy the control components' public keys file from the printing authority USB flash drive to the printing
authority offline PC at the location required by the U-Print application.
. Call the CHVote Business service manager to compare the hash of the installed file with the one of the file
held by the CHVote Business service manager.

:role: 4 distinct control component administrators pairs
include::role.part.adoc[]
[start=21]
. Go to a datacenter to gain access to a control component cluster node.
. Replace the old control components' public keys file with the new one obtained in step 11.
. Restart the control component application to take into account the new public keys file.
. Repeat steps 22 to 23 on the other datacenter.
. Repeat steps 21 to 24 for the other control components with different administrators pairs.
. Once all the control components have been updated with the new public keys, go to a datacenter
to gain access to a control component cluster node.
. Update the control component application environment variables with the new
private key file path.
. Restart the control component application to take into account the new environment variable.
. Repeat steps 27 to 28 on the other datacenter.
. Repeat steps 26 to 29 for the other control components with different administrators pairs.

==== Control components signature key pairs renewal

Follow the same steps than for the <<Control components signature key pairs creation>>
process.

==== Control components signature key pairs revocation

A signature key pair could be revoked for the following reasons:

- The signing private key has been compromised. All the data signed with
this private key cannot be trusted anymore, all current protocol instances
must be aborted.
- Planned key revocation after the key pairs renewal, to remove the old key pairs
once no active protocol instance runs with them.

:role: the CHVote IT Service manager
include::role.part.adoc[]
. Remove the revoked keys from the control components' public keys file and commit the changes
to the CHVote infrastructure git repository dedicated to the public keys. See
<<Control components' public keys format>> for the value format.

:role: actors defined for each referenced step
include::role.part.adoc[]
[start=2]
. Execute steps 8 to 20 from the <<Control components signature key pairs creation>> process.

:role: 4 distinct control component administrators pairs
include::role.part.adoc[]
[start=3]
. Go to a datacenter to gain access to a control component cluster node.
. Replace the old control components' public keys file with the new one.
. Remove the revoked private key.
. Repeat steps 4 to 5 on the other datacenter.
. Repeat steps 3 to 6 for the other control components with different administrators pairs.

TIP: In case of the revocation of an active private key, the
<<Control components signature key pairs creation>> process must then be
executed for the concerned control component to create a new key pair.

=== CHVote platform signature key pair processes

==== General description

The CHVote signature public key must be provided to the control components. This is implemented using a simple
hand-to-hand handover process between the CHVote System administrator and the 4 control components administrators pairs.

==== CHVote platform signature key pair creation

:role: the System administrator
include::role.part.adoc[]
. Execute a command line involving the PACT application binary to execute the key pair
generation console application. The keys are generated to the console output.
. Copy the output of the private key to an OCP secret configuration value whose access is restricted to a role
not given to the BackOffice application.
. Copy the output of the public key to a USB flash drive.
. Meet with each control component administrators pair to give them the public key hand-to-hand.

:role: 4 distinct control component administrators pairs
include::role.part.adoc[]
[start=5]
. Meet with the System administrator and copy the public key from the System administrator USB flash drive to the
control component administrators pair USB flash drive.
. Copy the public keys file onto the control component administrators USB flash drive.
. Go to one datacenter and connect to one node of a control component cluster
(using the physical control access and then logging to the local console)
. Connect the control component administrators USB flash drive.
. Update the local the public keys file (see <<CHVote platform signature public keys format>>  for the value format)
with the new public key from the USB flash drive.
. Update the environment variable pointing to the CHVote platform public keys file.
. Restart the control component application to take into account the environment variable.
. Repeat steps 8 to 11 for the other datacenter.
. Repeat steps 7 to 12 for the other control components with different administrators pairs.

==== CHVote platform signature key pair renewal

Follow the same steps than for the <<CHVote platform signature key pair creation>>
process.

==== CHVote platform signature key pair revocation

A signature key pair could be revoked for the following reasons:

- The signing private key has been compromised. All the data signed with
this private key cannot be trusted anymore, all the current protocol instances
must be aborted.
- Planned key revocation after a key pair renewal, to remove the old key pair
once no active protocol instance runs with them.

:role: the System administrator
include::role.part.adoc[]
. Send an email to the control component administrators to signal the public key to remove.

:role: 4 distinct control component administrators pairs
include::role.part.adoc[]
[start=2]
. Get the public keys file from the CHVote infrastructure git repository dedicated to the public keys.
. Copy the public keys file onto the control component administrators USB flash drive.
. Go to one datacenter and connect to one node of a control component cluster
(using the physical control access and then logging to the local console).
. Remove the revoked public key from the local the public keys file
(see <<CHVote platform signature public keys format>>  for the value format).
. Update the environment variable pointing to the CHVote platform public keys file.
. Restart the control component application to take into account the new
environment variable.
. Repeat steps 5 to 7 for the other datacenter.
. Repeat steps 3 to 8 for the other control components with different administrators pairs.

TIP: In case of the revocation of an active private key, the
<<CHVote platform signature key pair creation>> process must then be
executed to create a new key pair.

=== Printing authority secured channel key pairs processes

The following processes equally apply to both printing authority encryption or signature key pairs.

==== Global description

The users of the offline U-Print application, who are in charge to generate their own public keys,
have to provide them to the CHVote IT service manager. Concretely, this can be implemented using
one of those two possibilities:

* either the public key file is signed in a dedicated tool then the signed file is attached in an email to
the CHVote IT service manager,
* or the public key file is attached to an email that is then signed by the email application and sent to
the CHVote IT service manager.

When receiving the email, the CHVote IT service manager verifies the signature (at file or email level)
and add or modify the public key in the CHVote infrastructure git repository dedicated to the public keys.
The access to this repository is restricted by the CHVote internal IAM solution, ensuring that only the CHVote
IT service manager can modify this repository. The CHVote Service manager must then send an email to the CHVote
System administrator to signal the change.

The System administrator has read-only access on the CHVote infrastructure git repository dedicated to the public keys
so that he can download the public key to be configured into the CHVote platform.

==== Printing authority key pair creation

:role: the Printing authorities
include::role.part.adoc[]
. Generate the printing authority key pair using the U-Print application on an offline PC.
The key pair must be directly stored on a dedicated USB flash drive. The private key is protected
in a keystore using a password asked by the U-Print application.
. Send a signed email with the new public key to the CHVote IT Service manager and to the Election manager
with which the printing authority is in contract.

:role: the CHVote IT Service manager
include::role.part.adoc[]
[start=3]
. Verify the signature of the received email containing the printing authority public key.
. Add the public key to the file listing the valid keys for the printing authority
(see <<Printing authority encryption or signature public key format>> for the file content format)
and commit the changes to the CHVote infrastructure git repository dedicated to the public keys.
. Send an email to the System administrator to signal the change.

:role: the System administrator
include::role.part.adoc[]
[start=6]
. Get the public keys file from the CHVote infrastructure git repository dedicated to the public keys.
. Update the printing authority template value in the BackOffice database with the new key.

:role: the Election managers
include::role.part.adoc[]
[start=8]
. Verify the signature of the received email containing the printing authority public key.
. When deploying an operation configuration within the PACT application, check that the displayed
hash of the printer authority public key matches the hash of the received public key.

==== Printing authority key pair renewal

Follow the same steps than for the <<Printing authority key pair creation>>
process.

==== Printing authority key pair revocation

A printing authority key pair could be revoked for the following reasons:

- The private key has been compromised. All the data encrypted or signed with
this private key cannot be considered secret or authentic anymore, all the current protocol instances
must be aborted.
- Planned key revocation after a key pair renewal, to remove the old key pairs
once no active protocol instance runs with them.

:role: the Printing authorities
include::role.part.adoc[]
. Send a signed email to the CHVote IT Service manager with the public part of the compromised key pair.

:role: the CHVote IT Service manager
include::role.part.adoc[]
[start=2]
. Verify the signature of the received email containing the printing authority public key.
. Remove the public key from the file listing the valid keys for the printing authority
(see <<Printing authority encryption or signature public key format>> for the file content format)
and commit the changes to the CHVote infrastructure git repository dedicated to the public keys.

=== Electoral authority keys processes

==== Global description

The users of the offline U-Count application, who are in charge to generate their own keys,
have to provide the public key to the CHVote BackOffice. Concretely, the public key file is signed
in a dedicated tool then the signed file is uploaded to the BackOffice.

For the private keys, key shares are generated for each electoral authority using an n-out-of-m
Shamir's secret sharing scheme. Each share is protected by a user chosen password in a PKCS#12
key store file, and this file is stored on a USB flash disk owned by an electoral authority and
optionally on a CD-ROM for backup.

==== Electoral authority keys generation

:role: the Election Officer
include::role.part.adoc[]
[start=1]
. Insert the election officer USB flash drive.
. Start the U-Count application, start the key generation feature and set the output of the public
key to the election officer USB flash drive.

:role: m Electoral authorities
include::role.part.adoc[]
[start=3]
. Insert the electoral authority USB flash drive into the Counting offline PC when
prompted by the application.
. Set the output of the private key to the electoral authority USB flash drive when prompted
by the application.
. Type in the password twice when prompted by the application.
. Disconnect the electoral authority USB flash drive when the application confirms that the private
key is stored.
. Repeat steps 2 to 6 with the other electoral authorities and their respective USB flash drives.

:role: the Election Officer
include::role.part.adoc[]
[start=8]
. Disconnect the election officer USB flash drive when the application confirms that the public
key is stored.
. Shutdown the U-Count application and the counting offline PC.
. Plug the the election officer USB flash drive on an administrative PC.
. Sign the pkcs#12 key store file with the election officer X509 certificate.
. Upload the file into the BackOffice.

==== Electoral authority keys renewal

Follow the same steps than for the <<Electoral authority keys generation>> process.

==== Electoral authority keys revocation

No revocation can occur for the electoral authority keys. In case of compromise of the private key,
the protocol instance must be aborted.