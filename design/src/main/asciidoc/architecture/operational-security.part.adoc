= Operational Security

== Input validation

Each application endpoint must validate the input it receives, whether the input comes from
an untrusted zone or from the same trust zone (defense in depth). The goal of the input validation is
to ensure that the endpoint cannot receive any specifically crafted message that would
trigger a vulnerability in the target software (XSS, remote code execution, etc.).

Validation must take into account:

- Simple textual values: format, length, valid regular expression
- HTML content (rich text boxes): regular expression with the authorized entities (whitelisting is mandatory in this case)
- Complex textual content (json or xml)
    * xsd for xml documents
    * validators on the bounded bean for json content
- Files (PDF, images)
    * file type enforcement
    * file size limit
    * antivirus scan
    * sanitization

Implementation rules

- Whitelisting must be preferred to blacklisting whenever possible.
- JSON payload validations must be written as a priority with the standard Bean Validation annotations.
- Every validation code must be written with the matching security test (unit, integration or system test).

NOTE: The input validation described here only targets security objectives. Business validations should also be
conducted on a per-case basis, whose objectives are to validate the semantics of the data. Defense in depth
is not mandatory in this case, but the business validations should at least be conducted at the user
interaction level (client AND server-side).