= Capacity requirements

== Hypothesis

To make realistic scenarios, we must take into account the global vote
participation and the ratio of users using the internet voting channel
rather than other channels.

Optimistic hypothesis (maximum expected ratios):

* Global vote participation: 60%
* Ratio of internet voting: 30%
* Internet voting vote participation: 18%

The control components must be sized to allow concurrent protocols to
run for the client cantons according to the forecast by 2021.

The characteristics of each canton are expected to be the following:

.Expected canton characteristics

[width="100%",cols="17%,>29%,>28%,>26%",options="header",]
|=======================================================================
|Canton |Number of resident voters |Number of abroad voters |Election
set worst-case
|Aargau |350'000 |10'000 |
|Bern |680'000 |16'000 |
|Geneva |235'000 |25'000 |GC-CE: 7 out of 40 + 100 out of 730
|Luzern |230'000 |5'000 |
|St. Gallen |300'000 |8'000 |
|Vaud |380'000 |25'000 |
|Valais |180'000 |8'000 |
|*Total* |*2'355'000* |*97'000* |
|=======================================================================

== Business scenario #1: Simple sample

* Election set:
** 2 cantonal votation objects (1 out of 3)
** 1 municipal election (2 out of 10)
* Security level = 2
* Number of votes = 18 * number of voters / 100

== Business scenario #2: Geneva GC-CE elections

This scenario corresponds to the currently identified actual worst case.

* Election set:
** 1 cantonal election (7 out of 40)
** 1 cantonal election (100 out of 730)
* Security level = 2
* Number of votes = 18 * number of voters / 100

== Business performance targets

What is the worst case scenario that we must take into account for sizing the control components?
Sizing against a GC_CE scenario for all the cantons at the same time will lead to oversize the
infrastructure, which will in turn result in a prohibitively costly solution. On the other hand, we must ensure
that the platform is capable of running smaller elections in parallel to a big one like GC_CE.

We suggest setting the following goals:

_Upper bound:_ in case of the extremely unlikely scenario where all cantons run concurrently operations as complex
as GC_CE (with all the same milestones!), the control components should be able to process the following phase with
those properties:

.Upper bound goals
|======================
|Phase|Data set size|Max time spent

|Initializing the system|2'452'000 voters|24 hours
|Mixing and decrypting the ballot box|441'360 voters|18 hours (starts at 12:30 on saturday, ends at 6:30 on sunday morning)
|======================

_Lower bound:_ in case of the more likely scenario where all cantons run concurrently operations as simple
as SIMPLE_SAMPLE (still with the same milestones), the control components should be able to process the following phase
with those properties:

.Lower bound goals
|======================
|Phase|Data set size|Max time spent

|Initializing the system|2'452'000 voters|12 hours
|Mixing and decrypting the ballot box|441'360 voters|8 hours (starts at 12:30 on saturday, ends at 20:30 on saturday)
|======================

== Hardware sizing requirements

The requirements have been computed with an early version of the implementation
of the voting protocol and exclusively on x86 servers.

=== CPU cores and memory
Each control component server must provide the following hardware:

* At least 20 x86 cores for the application, 4 x86 cores for the database, or
the equivalent in the Power architecture.
* At least 200 GB RAM for the application, 32 GB for the database.
* As much frequency as affordable (impact on the OT response time to the voter).

=== Storage

==== Local storage

Local storage is used to host the control components' operating system, the application
and the private keys.

The system requires a 512 GB RAID 1 array.

==== SAN storage

The SAN storage is used for the following data:

- Control components PostgreSQL databases data (actual data and write ahead logs)
- CHVote Platform Oracle database (actual data, transaction logs, redo logs...)
- Applications logs (rotating)
- Operating system swap if the local disk array is not sufficient
- Database backups

==== Database volumetry

Running the elections for 250'000 voters and 45'000 votes generates the following database
volumetry (all control components + CHVote platform):

.Measured database volumetry for 250k voters
[width="100%",cols="<,>,>",options="header"]
|========
|Business case | Real operation data | Test site data
|SIMPLE_SAMPLE | 200 GB              | 1 GB
|GC_CE         | 800 GB              | 4 GB
|========

The data is automatically erased within 3 months if no recourse is under scrutiny, but:

- 2 operations could be live within the 3 months time frame (in preparation, running,
  archived but still not erased);
- 1 additional operation under recourse should be pessimistically provisioned.

The total estimate of data is projected for 3 operations each having a total of 2'500'000 voters and
450'000 votes, with a 50%-50% distribution among the voters of the business cases. The required
volumetry on disks is twice the measured volumetry because of their redundancy between the two data centers.

.Total volumetry of database
[width="100%",cols="<,^,^,>",options="header,footer"]
|========
|Business case    | Electorate estimation | Number of voters and votes | Data volumetry
|SIMPLE_SAMPLE    | 50%                   | 1'250'000 - 225'000        | 3 x 5 x 2 x 201 GB +
=  6 TB
|GC_CE            | 50%                   | 1'250'000 - 225'000        | 3 x 5 x 2 x 804 GB +
= 24 TB
|Total live data  | 100%                  | 2'500'000 - 450'000        | *30 TB*
|========

Furthermore, the database data must be backed up, using a snapshot method with 4 weeks retention.
Its volumetry is estimated at 30% of the live data volumetry, so *adding 9 TB* to the requirements.

==== Other volumetries

For each component, the following additional volumetries are required:

- Application logs: 256 GB
- Operating system swap: 512 GB (twice the RAM)

Thus, for the 8 control components, the data volumetry is *6 TB*.

For the CHVote platform applications (two OCP working nodes), *1.5 GB* are required .

==== Two mirrored infrastructures

As the control component infrastructure is doubled in the same way the CHVote platform is doubled for
enabling short maintenance windows, the required data volumetry on the SAN is the double of the volumetries
mentioned beforehand.

==== Total required volumetry

The projected required volumetry is detailed in the following table.

.Total required volumetry
[width="100%",cols="<,>",options="header,footer"]
|========
|Data | Volumetry
|Database live data | 30 TB
|Database backup data | 9 TB
|Swap and logs 8 control components | 6 TB
|Swap and logs 2 OCP worker nodes | 1.5 TB
|Total for a single infrastructure | 46.5 TB
|*Total for the two infrastructures* | *93 TB*
|========
